# Copyright 1999-2008 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

inherit qt3 autotools

MY_P=${P/dbus/dbus-1}

DESCRIPTION="D-BUS Qt3 bindings compatible with old application API and new dbus"
HOMEPAGE="http://freedesktop.org/wiki/Software/dbus"
SRC_URI="http://people.freedesktop.org/~krake/dbus-1-qt3/${MY_P}.tar.gz"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~alpha ~amd64 ~arm ~hppa ~ia64 ~mips ~ppc ~ppc64 ~sparc x86 ~x86-fbsd"
IUSE=""

RDEPEND=">=sys-apps/dbus-1.0"
DEPEND="${RDEPEND}
	=x11-libs/qt-3*"


S=${WORKDIR}/${MY_P}

src_install() {
        emake DESTDIR="${D}" install || die "emake install failed"
}
