# Copyright 1999-2009 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header:  $

inherit autotools
inherit git

DESCRIPTION="Static analysis for C"
HOMEPAGE="http://repo.or.cz/w/smatch.git"
EGIT_REPO_URI="git://repo.or.cz/smatch.git"

KEYWORDS=""

LICENSE="OSL-1.1"
SLOT="0"
IUSE=""

DEPEND=""
RDEPEND="!sys-devel/sparse"

src_compile() {
	sed -i \
		-e '/^PREFIX=/s:=.*:=/usr:' \
		-e "/^LIBDIR=/s:/lib:/$(get_libdir):" \
		Makefile || die
		epatch "${FILESDIR}/${PN}-fix-compiler-warnings.patch"
}

src_install() {
	emake DESTDIR="${D}" install || die "make install failed"
	dodoc FAQ README README-smatch
}
