# Copyright 1999-2007 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

inherit eutils autotools

DESCRIPTION="OPAL functional programming language"
HOMEPAGE="http://uebb.cs.tu-berlin.de/~opal/"
SRC_URI="http://uebb.cs.tu-berlin.de/~opal/ocs-${PV}-src.tar.bz2"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="-amd64 ~sparc ~x86"

IUSE="X doc +tk +tcl"

DEPEND=">=sys-libs/readline-4.3-r5
	>=sys-libs/ncurses-5.4-r5
	doc?       ( >=app-text/tetex-2 )
	tk?        ( >=dev-lang/tcl-8.4.6 )
	tcl?       ( >=dev-lang/tk-8.4.6-r1 )
	"

S="${WORKDIR}/ocs"

src_unpack() {
	unpack ${A}
	cd "${S}"

	#epatch "${FILESDIR}/ocs_2.3k-makesystem.diff"
	eautoreconf
}


src_compile() {
	mkdir -p "${D}"

	econf \
		$(use_enable doc) \
		$(use_enable doc dosfop) \
		$(use_enable tk opalwin) \
		$(use_enable tcl oasys) \
		--enable-dynamic \
		--disable-absolute-pathes \
		--enable-reflections \
		--enable-tivi2 \
		--enable-oc5 \
		--enable-proofchecker \
		--disable-locallinks \
		--prefix="${D}/usr" \
		|| die "./configure failed"
}

src_install() {
	# chmod:     /var <-- Prevent access violation
	addpredict /var
	mkdir -p "${D}/usr/"

	emake -j1 install || die "make failed"
}
