# Copyright 1999-2010 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header:  $

EAPI="2"

inherit distutils eutils

DESCRIPTION="Gentoo Kernel Security"
HOMEPAGE="http://dev.gentoo.org/~asym/guide.xml"

if [[ ${PV} == "9999" ]] ; then
	inherit git
	EGIT_REPO_URI="git://git.overlays.gentoo.org/proj/kernel-check.git"
	KEYWORDS=""
else
	SRC_URI="http://dev.gentoo.org/~asym/${P}.tar.bz2"
	KEYWORDS="~amd64 ~x86"
fi

LICENSE="LGPL-2.1"
SLOT="0"
IUSE="extras"

DEPEND=""
RDEPEND=""

DOCS="TODO"

src_unpack() {
	if [[ ${PV} == "9999" ]] ; then
		git_src_unpack
	else
		unpack ${A}
	fi
}

src_prepare() {
	if use extras; then
		epatch "${FILESDIR}/${PN}-extras.patch"
	fi
}

src_install() {
	distutils_src_install
}
